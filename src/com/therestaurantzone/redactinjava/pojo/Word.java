package com.therestaurantzone.redactinjava.pojo;

import com.itextpdf.text.pdf.parser.Vector;

public class Word {

  private float beginningTextCoordX;
  private float beginningTextCoordY;
  private int pageNumber;
  private Vector endingVector;
  private StringBuilder currentWord;
  private boolean flaggedForParsing = false;
  
  public Word() {
    this.currentWord = new StringBuilder();
  }
  
  public void setBeginningTextCoords(Vector start) {
    this.setBeginningTextCoordX(start.get(0));
    this.setBeginningTextCoordY(start.get(1));
  }
  
  public int length() {
    return this.currentWord.toString().length();
  }
  
  @Override
  public String toString() {
    return this.currentWord.toString();
  }
  
  public void appendCharToCurrentWord(String currentChar) {
    this.currentWord.append(currentChar);
  }

  public float getBeginningTextCoordX() {
    return this.beginningTextCoordX;
  }

  public void setBeginningTextCoordX(float beginningTextCoordX) {
    this.beginningTextCoordX = beginningTextCoordX;
  }

  public float getBeginningTextCoordY() {
    return this.beginningTextCoordY;
  }

  public void setBeginningTextCoordY(float beginningTextCoordY) {
    this.beginningTextCoordY = beginningTextCoordY;
  }
  
  public void setEndingVector(Vector endingVector) {
    this.endingVector = endingVector;
  }
  
  public float getEndingTextCoordX() {
    return this.endingVector.get(0);
  }
  
  public float getEndingTextCoordY() {
    return this.endingVector.get(1);
  }
  
  public void setPageNumber(int pageNumber) {
    this.pageNumber = pageNumber;
  }
  
  public int getPageNumber() {
    return this.pageNumber;
  }

  public boolean isFlaggedForParsing() {
    return this.flaggedForParsing;
  }

  public void setFlaggedForParsing(boolean flaggedForParsing) {
    this.flaggedForParsing = flaggedForParsing;
  }
}
