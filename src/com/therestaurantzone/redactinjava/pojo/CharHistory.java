package com.therestaurantzone.redactinjava.pojo;

import java.util.ArrayDeque;
import com.itextpdf.text.pdf.parser.Vector;

public class CharHistory {

  //Use Getters/Setters even for defaults;
  //Queue carrying previousChar and previousPreviousChar
  private ArrayDeque<Character> previousChar = new ArrayDeque<Character>();
  
  //private char previousChar;
  private Vector startingVectorOfPreviousChar;
  private Vector endingVectorOfPreviousChar;
  
  //SubClasses need to be at the end of the file so they can be found readily
  public CharHistory() {
    this.previousChar.add('\0');
    this.previousChar.add('\0');
  }
    
  //Push in history, removes outdated history
  //if you breakline to make it readable, unsure that there is noting on with toes based on their roles
  public void pushIntoHistory(Vector startingVectorOfCurrentChar,
      Vector endingVectorOfCurrentChar, char currentChar) {
    this.startingVectorOfPreviousChar = startingVectorOfCurrentChar;
    this.endingVectorOfPreviousChar = endingVectorOfCurrentChar;
    this.previousChar.poll();
    this.previousChar.add(currentChar);
  }
  
  
  // GETTERS and SETTERS
  public Vector getStartingVectorOfPreviousChar() {
    return this.startingVectorOfPreviousChar;
  }

  public Vector getEndingVectorOfPreviousChar() {
    return this.endingVectorOfPreviousChar;
  }

  public char getPreviousChar(int position) {
    if (position == 0) {
      return this.previousChar.peek();
    } else if (position == 1) {
      return this.previousChar.peekLast();
    }
    throw new IndexOutOfBoundsException("Expecting val 0 or 1, got " + position);
  }  
}
