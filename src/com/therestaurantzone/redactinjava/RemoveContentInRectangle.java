package com.therestaurantzone.redactinjava;

/*
 * Edited by Mark Carter to add support for using
 * an ArrayList of points for clearing multiple areas
 *
 * Example written by Bruno Lowagie in answer to:
 * http://stackoverflow.com/questions/27905740/remove-text-occurrences-contained-in-a-specified-area-with-itext
 * 
 */


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.pdfcleanup.PdfCleanUpLocation;
import com.itextpdf.text.pdf.pdfcleanup.PdfCleanUpProcessor;
import com.therestaurantzone.redactinjava.pojo.Word;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RemoveContentInRectangle {
  
  private PdfReader reader;
  private PdfStamper stamper;
  private String src;
  private String dest;
  
  public RemoveContentInRectangle(String src, String dest) throws IOException, DocumentException {
    this.src = src;
    this.dest = dest;
    this.reader = new PdfReader(this.src);
    this.stamper = new PdfStamper(reader, new FileOutputStream(dest));
  }
  
    public void manipulatePdf(ArrayList<Word> listOfFoundWords) throws IOException, DocumentException {
      
        List<PdfCleanUpLocation> cleanUpLocations = new ArrayList<PdfCleanUpLocation>();
        
        for (Word word : listOfFoundWords) {

          cleanUpLocations.add(new PdfCleanUpLocation(word.getPageNumber(), new Rectangle(word.getBeginningTextCoordX(), 
                                    word.getBeginningTextCoordY() - 3, 
                                    word.getEndingTextCoordX(), 
                                    word.getEndingTextCoordY() + 3), BaseColor.WHITE));
          if (word.isFlaggedForParsing()) System.out.println(word.toString() + " found in " + this.src.toString());
        }
        
        try {  //try with resources <-- reader, stamper do not implement java.lang.AutoCloseable
          PdfCleanUpProcessor cleaner = new PdfCleanUpProcessor(cleanUpLocations, stamper);
          cleaner.cleanUp(); //add close for cleaner <-- no close method
        } catch (Exception e) {
          if (!handledWrappedRuntimeException(e, this.src.toString())) throw e;
          e.printStackTrace();
        } finally {
          if (stamper != null) stamper.close();
          if (reader != null) reader.close();
        }
    }

    private boolean handledWrappedRuntimeException(Exception e, String fileName) {
      Throwable cause;
      while ((cause = e.getCause()) != null) {
        e = (Exception) cause;
      }
      if (e instanceof com.itextpdf.text.exceptions.UnsupportedPdfException) {
        System.out.println(fileName + " has UnsupportedPdfException");
        return true;
      } else if (e instanceof org.apache.commons.imaging.ImageReadException) {
        System.out.println(fileName + " has ImageReadException");
        return true;
      }
      return false;
    }
}