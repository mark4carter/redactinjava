package com.therestaurantzone.redactinjava.extraction;

/**
 * TextLocationExtractionStrategy
 * 
 * @author Mark Carter
 * -- Complete rewrite, iText SimpleTextExtractionStrategy used as base
 * 
 *    Used to find occurrences of a regex and to determine the coordinates
 *     - currently used for rectangle redaction
 */

import java.util.ArrayList;

import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.LineSegment;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.parser.Vector;
import com.therestaurantzone.redactinjava.BatchRedactOp;
import com.therestaurantzone.redactinjava.pojo.CharHistory;
import com.therestaurantzone.redactinjava.pojo.Word;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class TextLocationExtractionStrategy implements LocationExtractionStrategy {

  private static final Logger logger = LogManager.getLogger(BatchRedactOp.class);

  //Organizational Tidbit
  //Set the defaults in the getter methods instead of at the top of the file
  /*Use Getter/Setters for every class variables (obiovusly you cannot use it in method variables).
   * public Word getWord(){
   * if(word == null){
   *   this.setWord(new Word());
   *   }
   * return word;
   * }
   *
   */
  private Word word = new Word();
  private CharHistory charHistory = new CharHistory();

  //What is Coords? why not textCoordinates ?
  private ArrayList<Word> textCoords = new ArrayList<Word>();
  //Can we get this to be parameter read via system or via a property so we can update on the fly?
  private ArrayList<String> listOfRegexToRedact;

  private boolean checkingForPhoneSequence = false;
  private boolean renderingFirstCharOfWord = true;

  private static final String EOL_REGEX = "[,\\[\\]:;!?�]";  
   
  public TextLocationExtractionStrategy(ArrayList<String> listOfRegexToRedact) {
	  //Use a Getter/Setter instead of directly accessing the value.
    this.listOfRegexToRedact = listOfRegexToRedact;
  }
  
  @Override
  public ArrayList<Word> getTextCoords(int pageNumber, ArrayList<Word> passedTextCoords ) {
	  //I may need a refresher on what endWord is actually achieving. It seems like it is
	  //simply doing the reset of this class for the next word.
	  //If that is the case you should look to edit the Constructors for your classes so that
	  //you can easily call the class again and get rid of unnecessary functions.
    endWord();
    //Getters/Setters
    for (Word textCoord : this.textCoords) {
      textCoord.setPageNumber(pageNumber);
      passedTextCoords.add(textCoord);
    }
    return passedTextCoords;
  }
  
  @Override
  public void renderText(TextRenderInfo renderInfo) {

    for (TextRenderInfo charRender : renderInfo.getCharacterRenderInfos()) {
      String currentCharString = charRender.getText();
      //Why not an else, if its greater//equal to 1 then it will never be less than/equal to 0
      //it would be more readable and smoother.
      if (currentCharString.length() <= 0) continue;
      if (currentCharString.length() >= 1) currentCharString = Character.toString(currentCharString.charAt(0));
      
      logger.debug("[" + charRender.getText() + "]");
      //Wrap boolean assignments in ()
      //boolean firstRender = ( word.length == 0 );
      boolean firstRender = word.length() == 0;
      boolean hardReturn = false;
      LineSegment segment = charRender.getBaseline();
      //Get rid of Vectors
      //http://javahungry.blogspot.com/2013/12/difference-between-arraylist-and-vector-in-java-collection-interview-question.html
      Vector startingVectorOfCurrentChar = segment.getStartPoint();
      Vector endingVectorOfCurrentChar = segment.getEndPoint();

      char c = charRender.getText().charAt(0);
      // System.out To display coordinates of current Char
      //System.out.print("[" + startingVectorOfCurrentChar.get(0) + "," +
                    //startingVectorOfCurrentChar.get(1) + "] [" + c +"]"); 

      //Check to see if Character is on the same line as last Character
      //  using matchCharLine method
      if (!firstRender) {
        hardReturn = isCharOnSamelineAsPrevious(startingVectorOfCurrentChar,
                           charHistory.getStartingVectorOfPreviousChar(),
                           charHistory.getEndingVectorOfPreviousChar(), 
                           charRender);
      }

      //move the early exists that are fastest to the front, for instance seeing if c or character.isSpacheChar(EOL_REGEX)
      //should be at the bottom of the if statement as it will take the most time and JAVA should exit earlier
      if (hardReturn || currentCharString.matches(EOL_REGEX)
                    || Character.isSpaceChar(c)
                    || (c == 9)) {
        if (!(isThisWhiteSpacePartOfPhoneNumber(charHistory.getPreviousChar(0),
                             charHistory.getPreviousChar(1)))) {
        	//At least add a this. or keew 
          endWord();
          if (!currentCharString.matches(EOL_REGEX) && (!Character.isSpaceChar(c))) {
            setWordCoord(currentCharString, startingVectorOfCurrentChar);
          }
        }
      }  else {
    	  //always use this.classMethod() for class methods for clarity.
    	  //http://stackoverflow.com/questions/1568091/why-use-getters-and-setters
        if (firstRender) setWordCoord(currentCharString, startingVectorOfCurrentChar);
        if (!firstRender) {
          // spacing could be (lastEnd.get(0) - start.get(0))
          float spacing = charHistory.getEndingVectorOfPreviousChar()
              .subtract(startingVectorOfCurrentChar).length();
          //System.out.print("[s" + spacing + "][sw" + charRender.getSingleSpaceWidth() + "] " + "|" + c + "|");
          //change the early exit case to the quickest set - if(spacing < 0 || ...)
          if ((spacing > charRender.getSingleSpaceWidth()/3.5f || spacing < 0)
              && (!checkingForPhoneSequence)){
            logger.trace("<HS>");
            endWord();
            setWordCoord(currentCharString, startingVectorOfCurrentChar);
          } else if (renderingFirstCharOfWord ){
            setWordCoord(currentCharString, startingVectorOfCurrentChar);
          } else {
            appendChar(currentCharString);
          }
        } 
      }
      //Again just use this.charHistory.pushIntoHistory(...)
      charHistory.pushIntoHistory(startingVectorOfCurrentChar, endingVectorOfCurrentChar, c);
    }
  }

  public boolean isCharOnSamelineAsPrevious(
      Vector x0, Vector x1, Vector x2, TextRenderInfo charRender) {
    //TODO: switch to trying Vector y1 minus Vector y2 to find if char is on different line
    //float dist = Math.abs(x0.get(1) - x1.get(1));
    float dist = (x2.subtract(x1)).cross((x1.subtract(x0))).lengthSquared()
                     / x2.subtract(x1).lengthSquared();

    float sameLineThreshold = 1f; 
    if (dist > sameLineThreshold){
        endWord();
      return true;
    }
    return false;
  }

  public void appendChar(String tChar) {
	  //use Getters/Setters to set class variables;
    word.appendCharToCurrentWord(tChar);
    renderingFirstCharOfWord = false;
  }

  public void setWordCoord(String tChar, Vector start) {
    word.appendCharToCurrentWord(tChar);
    word.setBeginningTextCoords(start);
    renderingFirstCharOfWord = false;
  }

  public void endWord() {
    logger.debug(word.toString() + "\n");
    if (!"".equals(word.toString())) {  //get String utils is not empty is a better approach
      for (String regexEntry : listOfRegexToRedact){
        if (word.toString().matches(".*" + regexEntry + ".*")){
          if (word.toString().indexOf('@') > 0) word.setFlaggedForParsing(true);
          word.setEndingVector(charHistory.getEndingVectorOfPreviousChar());
          textCoords.add(word);
        }
      }
    }
    word = new Word();
    renderingFirstCharOfWord = true;
    checkingForPhoneSequence = false;
  }
  

  /**
   * no-op method - this renderer isn't interested in image events
   * @see com.itextpdf.text.pdf.parser.RenderListener#renderImage
   * (com.itextpdf.text.pdf.parser.ImageRenderInfo)
   * @since 5.0.1
   */
  @Override
  public void renderImage(ImageRenderInfo renderInfo) {
      // do nothing - we aren't tracking images in this renderer
  }
  
  public boolean isThisWhiteSpacePartOfPhoneNumber(char lastC, char secondLastC) {
    // first if checks if last character is number and length is 3 or 6

    if ((word.length() == 3 || word.length() == 6) &&
          word.toString().matches("([0-9]{3})|([0-9]{6})")) {
      checkingForPhoneSequence = true;
      logger.trace("[t1]");
      return true;
    // Checks for 123) or 123. 123- and other variations
    } else if (word.toString().matches(".*[0-9]{3}[-\\)./��]")) {
      checkingForPhoneSequence = true;
      logger.trace("[t2]");
      return true;
    // Checks for prefix symbols such as +1123456
    } else if (word.toString().matches(".*([0-9]){9}")) {
      checkingForPhoneSequence = true;
      logger.trace("[t3]");
      return true;
    } else if ((!Character.isAlphabetic(lastC))
        && word.toString()
        .matches(".*\\(?([0-9]{3})\\)?/?[-.\\s]?([0-9]{3})[-.\\s/]?|\\(([0-9]{3})\\)[-.\\s/]?")) {
      checkingForPhoneSequence = true;
      logger.trace("[t4]");
      return true;
    }
    logger.trace("[f0]");
    return false;
  }
  
  @Override
  public void endPage() {
    textCoords = new ArrayList<Word>();
  }

  // Interface Requirement
  @Override
  public void beginTextBlock() {
  }

  // Interface Requirement
  @Override
  public void endTextBlock() {
  }
}
