package com.therestaurantzone.redactinjava.extraction;

import java.util.ArrayList;

import com.itextpdf.text.pdf.parser.RenderListener;
import com.therestaurantzone.redactinjava.pojo.Word;

public interface LocationExtractionStrategy extends RenderListener{

  //Interfaces should use the most abstract type that is possible. For instance I would use
  //public List<Word> getTextCoords(...)
  public ArrayList<Word> getTextCoords(int pageNumber, ArrayList<Word> passedTextCoords );
  
  public void endPage();
  
}
