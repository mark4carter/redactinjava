package com.therestaurantzone.redactinjava;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.therestaurantzone.redactinjava.extraction.LocationExtractionStrategy;
import com.therestaurantzone.redactinjava.pojo.Word;
/*
 * Complete rewrite of location strategy done by Mark Carter
* Edited by Mark Carter for splitting words
*   and running regex on words and while grabbing location
*    
*    @param pdf of original PDF
*    @param ArrayList of regex to search for
*    
*    Original from http://what-when-how.com/itext-5/parsing-pdfs-part-2-itext-5/
*  
*/

public class SimpleTextLocator { 
  
  private PdfReader reader = null;
  private PdfReaderContentParser parser = null;
  // LocationExtractionStrategy interface
  private LocationExtractionStrategy locationExtractionStrategy = null;
  
  public SimpleTextLocator(File fileEntry, LocationExtractionStrategy locationExtractionStrategy) throws IOException {
    this.reader = new PdfReader(fileEntry.toURI().toString());
    this.parser = new PdfReaderContentParser(reader);
    this.locationExtractionStrategy = locationExtractionStrategy;
  }
  
  // Method for getting file prepared to be parsed by TextLocationExtractionStrategy
  public ArrayList<Word> parseSingle() throws IOException {
    ArrayList<Word> foundWords = new ArrayList<Word>();
    RenderListener renderListener;
    for (int i = 1; i <= this.reader.getNumberOfPages(); i++) {
      //parses through the pdf to find coordinates
      renderListener = this.parser.processContent(i, this.locationExtractionStrategy);
      foundWords = ((LocationExtractionStrategy) renderListener).getTextCoords(i, foundWords);
      //copies found coordinates to textCoords
      this.locationExtractionStrategy.endPage();
    }  
    return foundWords;
  }
}