package com.therestaurantzone.redactinjava;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import com.itextpdf.text.DocumentException;
import com.therestaurantzone.redactinjava.extraction.TextLocationExtractionStrategy;
import com.therestaurantzone.redactinjava.pojo.Word;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * @author Mark Carter 
 *         redactSingle and fedactFolder gives the ability to redact portions of
 *         a pdf based of regex's placed in the fillArray method
 */

public class BatchRedactOp {
  
  private static final Logger logger = LogManager.getLogger(BatchRedactOp.class);

  private ArrayList<String> listOfRegexToRedact = new ArrayList<String>();
  
  private final static String EMAIL_REGEX = "[a-zA-Z0-9._%+-\\/]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}";
  private final static String PHONE_REGEX = "\\(?([0-9]{3})\\)?/?[-.\\s��]?([0-9]{3})[-.\\s/��]?([0-9]{4})";

  public static void main(String[] args) throws URISyntaxException, IOException, DocumentException{
    
    BatchRedactOp batchRedactOp = new BatchRedactOp();
    batchRedactOp.addToRegexList(EMAIL_REGEX);
    batchRedactOp.addToRegexList(PHONE_REGEX);
    
    ClassLoader classLoader = BatchRedactOp.class.getClassLoader();

    if (args.length > 1) {
      int filePrefix = 1;
      for (String fileArgs : args) {
        File file = new File(new URI(fileArgs));
        batchRedactOp.startRedact(file, filePrefix++);
      }
    } else if (args[0].equals("bottom")) {
      File file = new File (classLoader
          .getResource("bottomPile").toURI());
      batchRedactOp.startRedact(file);
    } else if (args[0].equals("resu4")) {
      File file = new File (classLoader
          .getResource("resu4").toURI());
      batchRedactOp.startRedact(file);
    } else if (args[0].equals("resu2")) {
      File file = new File (classLoader
          .getResource("resu2").toURI());
      batchRedactOp.startRedact(file);
    } else if (args[0].equals("errors")) {
      File file = new File(classLoader
          .getResource("errors").getFile());
      batchRedactOp.startRedact(file);
    } 
  }

  public void startRedact(File file, int... fileNum) throws IOException, DocumentException {
    int fileNo = 1;
    if (fileNum.length > 0) fileNo = fileNum[0];

    TextLocationExtractionStrategy tle = new TextLocationExtractionStrategy(listOfRegexToRedact);
    
    if (file.isFile()) {
      
      ArrayList<Word> listOfFoundWords = new SimpleTextLocator(file, tle).parseSingle();
      
      new RemoveContentInRectangle(file.toURI().toString(), "Test00" + fileNo + "S.pdf")
            .manipulatePdf(listOfFoundWords);
      
    } else if (file.isDirectory()) {
      for (File fileEntry : file.listFiles()) {
        startRedact(fileEntry, fileNo++);
      }
    } 
  }
  
  public void addToRegexList(String regexCheck) {
    listOfRegexToRedact.add(regexCheck);
  }
}
